//
//  ViewController.m
//  UIViewGeometryHomeWork
//
//  Created by Sergey on 15/08/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) UIView* centerView;
@property (weak, nonatomic) UIView* draggingView;
@property (assign, nonatomic) CGPoint offsetPoint;
@property (assign, nonatomic) CGPoint previousCenter;
@property (strong, nonatomic) NSMutableDictionary* freeSquare;
@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.freeSquare = [NSMutableDictionary dictionary];
    [self addcenterSquare];
    [self addSquare];
    [self addPawns];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Square

- (void)addcenterSquare {
    
    self.centerView = [[UIView alloc] init];
    self.centerView.backgroundColor = [UIColor brownColor];
    
    CGFloat centerWidth = MIN(self.view.frame.size.width, self.view.frame.size.height);
    
    self.centerView.frame = CGRectMake(
                                       (centerWidth / (CGFloat)2) - (centerWidth / (CGFloat)2)
                                       , (self.view.frame.size.height / (CGFloat)2) - (centerWidth / (CGFloat)2)
                                       , centerWidth
                                       , centerWidth
                                       );
    self.centerView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.centerView.tag = 10;
    [self.view addSubview:self.centerView];
}

- (void)addSquare {
    
    static int tagIndex = 101;
    
    CGFloat width = self.centerView.frame.size.width;
    CGFloat step = width / (CGFloat)4;
    CGFloat side = width / (CGFloat)8;
    
    for (int i = 0; i < 8; i++) {
        
        CGFloat dx = i % 2 ? 0 : (width / 8);
        CGFloat dy = i * side;
        
        for (CGFloat x = dx; x <= width - side; x+=step) {
            
            CGRect rect = CGRectMake(x, dy, side, side);
            UIView* view = [[UIView alloc] initWithFrame:rect];
            
            view.tag = tagIndex;
            view.backgroundColor = [UIColor blackColor];
            view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            
            [self.centerView addSubview:view];
            [self.freeSquare setObject:view forKey:[NSString stringWithFormat:@"%d", (int)[view tag]]];
            
            ++tagIndex;
        }
    }
}

- (void)addPawns {
    
    static int tagIndex = 201;
    
    CGFloat width = self.centerView.frame.size.width;
    CGFloat step = width / (CGFloat)4;
    CGFloat side = width / (CGFloat)8;
    CGFloat size = 30;
    
    for (int i = 0; i < 8; i++) {
        CGFloat dx = (i % 2) ? ((side - size)/(CGFloat)2) : (side + (side - size)/(CGFloat)2);
        CGFloat dy = i * side + (side - size)/(CGFloat)2;
        for (CGFloat x = dx; x <= width - side/(CGFloat)2 - size/(CGFloat)2; x+=step) {
            
            CGRect rect = CGRectMake(x, dy, size, size);
            UIView* pawn = [[UIView alloc] initWithFrame:rect];
            pawn.tag = tagIndex;
            pawn.layer.cornerRadius = 15;
            pawn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            
            if ([pawn tag] <= 212) {
                pawn.backgroundColor = [UIColor redColor];
                [self.centerView addSubview:pawn];
                [self removeFreeSquareForPawn:pawn.center];
            }

            else if ([pawn tag] > 220) {
                pawn.backgroundColor = [UIColor grayColor];
                [self.centerView addSubview:pawn];
                [self removeFreeSquareForPawn:pawn.center];
            }

            ++tagIndex;
        }
    }
}

- (void) removeFreeSquareForPawn:(CGPoint) center {

    for (UIView* freeView in [self.freeSquare allValues]) {

        if (CGPointEqualToPoint(freeView.center, center)) {
            [self.freeSquare removeObjectForKey:[NSString stringWithFormat:@"%d", (int)freeView.tag]];
        }
    }
}

- (void) addFreeSquareForPawn:(CGPoint) point {
    
    for (UIView* square in [[self.view viewWithTag:10] subviews]) {
        if (CGPointEqualToPoint(square.center, point)) {
            [self.freeSquare setObject:square forKey:[NSString stringWithFormat:@"%d", (int)[square tag]]];
            return;
        }
    }
}

#pragma mark - MovingPawns

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    UITouch* touch = [touches anyObject];
    
    CGPoint point = [touch locationInView:self.view];
    
    UIView* view = [self.view hitTest:point withEvent:event];
    
    if (view.tag > 200) {
        
        self.draggingView = view;
        self.previousCenter = self.draggingView.center;
        [self addFreeSquareForPawn:self.previousCenter];
        
        [self.view bringSubviewToFront:self.draggingView];
        
        CGPoint touchPoint = [touch locationInView:self.draggingView];
        
        self.offsetPoint = CGPointMake(CGRectGetMidX(self.draggingView.bounds) - touchPoint.x
                                       , CGRectGetMidY(self.draggingView.bounds) - touchPoint.y);
        
        [UIView animateWithDuration:0.2 animations:^{
            self.draggingView.alpha = 0.8;
            self.draggingView.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }];
    }
    else {
        self.draggingView = nil;
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    if (self.draggingView) {
        UITouch* touch = [touches anyObject];
        CGPoint point = [touch locationInView:[self.view viewWithTag:10]];

        CGPoint correctionPoint = CGPointMake(point.x + self.offsetPoint.x
                                                  , point.y + self.offsetPoint.y);
        self.draggingView.center = correctionPoint;
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {

    [self putPawn:[touches anyObject] event:event];
    [self movingEnded];
}

- (void) touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self putPawn:[touches anyObject] event:event];
    [self movingEnded];
}

#pragma mark - action with Pawn

- (void)movingEnded {
    
    [UIView animateWithDuration:0.2 animations:^{
        self.draggingView.alpha = 1;
        self.draggingView.transform = CGAffineTransformIdentity;
    }];
    
    self.draggingView = nil;
}

- (void) putPawn:(UITouch*)touch event:(UIEvent*) event {

    // Если вышли за границы доски
    CGPoint point = [touch locationInView:[self.view viewWithTag:10]];
    if (!CGRectContainsPoint([self.view viewWithTag:10].bounds, point)) {
        [UIView animateWithDuration:0.3 animations:^{
            self.draggingView.center = self.previousCenter;
            [self removeFreeSquareForPawn:self.previousCenter];
        }];
        return;
    }


    // Если переместили фигуру на пустую клетку
    for (UIView* square in [self.freeSquare allValues]) {
        if (CGRectContainsPoint(square.frame, point)) {
            [UIView animateWithDuration:0.3 animations:^{
                self.draggingView.center = square.center;
            }];
            [self addFreeSquareForPawn:self.previousCenter];
            [self removeFreeSquareForPawn:self.draggingView.center];
            return;
        }
    }

    // Если поставили не на черное поле/на другую фигуру перемещаем на ближайшую свободную
    UIView* nextSquare = [self squareWithMinimumDistanceForPawn:self.draggingView];
    if (nextSquare != nil) {

        [self addFreeSquareForPawn:self.previousCenter];
        [self removeFreeSquareForPawn:nextSquare.center];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.draggingView.center = nextSquare.center;
        }];
    }
}

- (UIView*) squareWithMinimumDistanceForPawn:(UIView*) draggingView {

    double minimumDistance = (double)INT_MAX;
    UIView* square = nil;

    for (UIView* view in [[self freeSquare] allValues]) {

        double currentDistance = hypot((view.center.x - draggingView.center.x)
                                       , (view.center.y - draggingView.center.y));

        if (currentDistance < minimumDistance) {
            minimumDistance = currentDistance;
            square = view;
        }
    }
    return square;
}

@end









